Test case for back-end developer | Sponge
============================

CONFIGURATION
-------------

### Composer

Run this command in your terminal:

```
$ composer install
```

### Database

You can create database with next command or whatever you like:

```mysql
CREATE DATABASE `db_name` CHARACTER SET utf8 COLLATE utf8_general_ci;
```

You rename the file `.env.example` into `.env`.
Edit the file `.env` with real data, for example:

```php
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=db_name
DB_USERNAME=username
DB_PASSWORD=password
```

Run migrations to create tables in database:

```
$ php artisan migrate
```

Run seeds to to fill the created tables with data:

```
$ php artisan db:seed --class=TabsTableSeeder
$ php artisan db:seed --class=ServicesTableSeeder

```

Run the command to generate app_key:

```
$ php artisan key:generate
```
Access to the admin 

```
Login: admin@gmail.com
Password: secret
```


**NOTES:**
- Laravel won't create the database for you, this has to be done manually before you can access it.
