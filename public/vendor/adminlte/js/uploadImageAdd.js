$(function() {
    $('#image_add').change(function(){
        let file=this.files[0];
        let imageType = file.type;
        let extentions = ["image/jpeg","image/png","image/jpg","image/gif", "image/svg+xml"];

        if(extentions.indexOf(imageType) >= 0 ){
            toastr.success('File successfully download!', {timeOut: 5000});
            FileUploadAjax();
        }else {
            toastr.error('File Is Invalid!', {timeOut: 5000});
            $('#previewImage').attr('src','/images/no-image.png');
            return false;
        }
    });
});
function FileUploadAjax(){
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': token
        },
        url: 'servicies/upload_image',
        type:'POST',
        data: new FormData($('#uploadFile').get(0)),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
            $('#previewImage').attr('src',"/images/"+data.file);
        }, error:function (error) {
            console.log(error);
        }
    });
}

$(function() {
    $('#image_edit').change(function(){
        let file=this.files[0];
        let imageType = file.type;
        let extentions = ["image/jpeg","image/png","image/jpg","image/gif", "image/svg+xml"];

        if(extentions.indexOf(imageType) >= 0 ){
            toastr.success('File successfully download!', {timeOut: 5000});
            FileUploadAjaxEdit();
        }else {
            toastr.error('File Is Invalid!', {timeOut: 5000});
            $('#previewImageEdit').attr('src','/images/no-image.png');
            return false;
        }
    });
});
function FileUploadAjaxEdit(){
    let token = $('meta[name="csrf-token"]').attr('content');

    $.ajax({
        headers: {
            'X-CSRF-TOKEN': token
        },
        url: 'servicies/upload_image',
        type:'POST',
        data: new FormData($('#uploadFileEdit').get(0)),
        contentType:false,
        cache:false,
        processData:false,
        success:function(data){
            $('#previewImageEdit').attr('src',"/images/"+data.file);
        },
        error:function (error) {
            console.log(error);
        }
    });
}