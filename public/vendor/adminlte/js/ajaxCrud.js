// Add tab
$(document).on('click', '.add-modal', function() {
    $('#addModal').modal('show');
});
$('.modal-footer').on('click', '.add', function() {
    $.ajax({
        type: 'POST',
        url: 'tabs',
        data: {
            '_token': $('input[name=_token]').val(),
            'name':  $('#name_add').val(),
            'description': $('#description_add').val(),
            'servicies': $('#servicies_add').val(),
        },
        success: function(data) {
            console.log(data);
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            toastr.success('Successfully add Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#addModal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.name) {
                    $('.errorName').removeClass('hidden');
                    $('.errorName').text(error.responseJSON.errors.name);
                }

                if (error.responseJSON.errors.description) {
                    $('.errorDescription').removeClass('hidden');
                    $('.errorDescription').text(error.responseJSON.errors.description);
                }
            }
        },
    });
});

// Edit tab
$(document).on('click', '.edit-modal', function() {
    $('.modal-title').text('Edit');
    $('#id_edit').val($(this).parent().parent().children("td#tab-id").text());
    $('#name_edit').val($(this).parent().parent().children("td#tab-name").text());
    $('#description_edit').val($(this).parent().parent().children("td#tab-description").text());

    let services = $(this).parent().parent().children("td#tab-services").text().split(',');

    $.each(services, function(key, value){
        $('.select2-search').before('<li class="select2-selection__choice" title='+ value + '>' +
            '<span class="select2-selection__choice__remove" role="presentation">×</span>'+ value + '</li>');
    });

    $('#editModal').modal('show');
});
$('.modal-footer').on('click', '.edit', function() {

    let id = $('#id_edit').val();
    let token = $('input[name=_token]').val();
    let name = $('#name_edit').val();
    let description = $('#description_edit').val();
    let services = $('#servicies_edit').val();

    $.ajax({
        type: 'PUT',
        url: 'tabs/' + id,
        data: {
            '_token': token,
            'id': id,
            'name': name,
            'description': description,
            'servicies': services
        },
        success: function(data) {
            console.log(data)
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');    // $('#description_edit').val($(this).data('description'));


            toastr.success('Successfully edit Tab!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);

        },
        error: function (error) {
            console.log(error)
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#editModal').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.name) {
                    $('.errorName').removeClass('hidden');
                    $('.errorName').text(error.responseJSON.errors.name);
                }

                if (error.responseJSON.errors.description) {
                    $('.errorDescription').removeClass('hidden');
                    $('.errorDescription').text(error.responseJSON.errors.description);
                }
            }
        },
    });
});

$(".delete").click(function(){
    let id = $(this).data("id");
    let token = $('input[name=_token]').val();
    let delete_item = $(this).parent().parent();
    let page= $(this).data("page");
    let img =  $('#img', delete_item).attr("src");

    $.ajax(
        {
            url: page + "/" + id,
            type: 'DELETE',
            dataType: "JSON",
            data: {
                '_token': token,
                "id": id,
                'img': img,
                "_method": 'DELETE',
            },
            success: function (msg) {
                if ( msg.status === 'success' ) {
                    toastr.success('Successfully deleted!', 'Success Alert', {timeOut: 1000});
                    delete_item.remove();
                }
            },
            error: function (error) {
                console.log(error);
            },
        });
});

// add service
$(document).on('click', '.add-modal-service', function() {
    $('#addModalService').modal('show');
});
$('.modal-footer').on('click', '.add-service', function() {

    let token = $('input[name=_token]').val();

    $.ajax({
        type: 'POST',
        url: 'servicies',
        data: {
            '_token': token,
            'title': $('#title_add').val(),
            'description': $('#description_add_service').val(),
            'image': $('#previewImage').attr('src')
        },
        success: function(data) {
            console.log(data);
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            toastr.success('Successfully add Service!', 'Success Alert', {timeOut: 1000});
            setTimeout(function () {
                window.location.reload();
            }, 1000);
        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#addModalService').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.description) {
                    $('.errorDescription').removeClass('hidden');
                    $('.errorDescription').text(error.responseJSON.errors.description);

                } if (error.responseJSON.errors.title) {
                    $('.errorTitle').removeClass('hidden');
                    $('.errorTitle').text(error.responseJSON.errors.title);
                }
            }
        },
    });
});

// Edit service
$(document).on('click', '.edit-modal-service', function() {
    $('.modal-title').text('Edit Service');
    $('#id_service_edit').val($(this).parent().parent().children("td#service-id").text());
    $('#title_service_edit').val($(this).parent().parent().children("td#service-title").text());
    $('#description_service_edit').val($(this).parent().parent().children("td#service-description").text());
    let image = $(this).parent().parent().children("td#service-image").children("img").attr('src');
    $('#previewImageEdit').attr('src', image);
    $('#editModalService').modal('show');
});
$('.modal-footer').on('click', '.edit-service', function() {

    let id = $('#id_service_edit').val();
    let token = $('input[name=_token]').val();
    let description = $('#description_service_edit').val();
    let title = $('#title_service_edit').val();
    let image = $('#previewImageEdit').attr('src');


    $.ajax({
        type: 'PUT',
        url: 'servicies/' + id,
        data: {
            '_token': token,
            'id': id,
            'description': description,
            'title': title,
            'image': image
        },
        success: function(data) {
            console.log(data);
            $('.errorTitle').addClass('hidden');
            $('.errorContent').addClass('hidden');

            toastr.success('Successfully edit Service!', 'Success Alert', {timeOut: 1000});
            $('#service-description').val(description);
            setTimeout(function () {
                window.location.reload();
            }, 1000);

        },
        error: function (error) {
            console.log(error);
            if ((error.responseJSON.errors)) {
                setTimeout(function () {
                    $('#editModalService').modal('show');
                    toastr.error('Validation error!', 'Error Alert', {timeOut: 5000});
                }, 500);

                if (error.responseJSON.errors.name) {
                    $('.errorName').removeClass('hidden');
                    $('.errorName').text(error.responseJSON.errors.name);
                }

                if (error.responseJSON.errors.description) {
                    $('.errorDescription').removeClass('hidden');
                    $('.errorDescription').text(error.responseJSON.errors.description);
                }
            }
        },
    });
});