<!-- Modal form to edit a form -->
<div id="editModalService" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" enctype="multipart/form-data" id="uploadFileEdit">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_service_edit" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Image:</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="image" id="image_edit" multiple>
                            <br>
                            <img src="" class="img-responsive" id="previewImageEdit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">Title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title_service_edit">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description_service_edit" cols="40" rows="5"></textarea>
                            <p class="errorDescription text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material edit-service" data-dismiss="modal">
                        <span class='glyphicon glyphicon-check'></span> Edit
                    </button>
                    <button type="button" class="btn btn-material" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>