<!-- Modal form to add a post -->
<div id="addModalService" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form" enctype="multipart/form-data" id="uploadFile">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Image:</label>
                        <div class="col-sm-10">
                            <input type="file" class="form-control" name="image" id="image_add" multiple>
                            <br>
                            <img src="" class="img-responsive" id="previewImage">
                            <p class="errorName text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Title:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="title_add" autofocus>
                            <p class="errorTitle text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description_add_service" cols="40" rows="5"></textarea>
                            <p class="errorDescription text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material add-service" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Add
                    </button>
                    <button type="button" class="btn btn-material" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
