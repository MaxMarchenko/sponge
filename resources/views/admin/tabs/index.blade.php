@extends('adminlte::page')

@section('title', 'Sponge Digital & Design')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
               <h2>Tabs</h2>
                <button class="btn btn-material add-modal">Create</button>
                <table id="dataTable" class="mdl-data-table" style="width:100%">
                   <thead>
                   <tr>
                       <th>ID</th>
                       <th>Name</th>
                       <th>Description</th>
                       <th>Services</th>
                       <th>Actions</th>
                   </tr>
                   </thead>
                   <tbody>
                   @foreach($tabs as $tab)
                       <tr>
                           <td id="tab-id">{{$tab->id}}</td>
                           <td id="tab-name">{{$tab->name}}</td>
                           <td id="tab-description">{{$tab->description}}</td>
                           <td id="tab-services">{{$tab->getServices()}}</td>

                           <td>
                               <button class="btn btn-material edit-modal js-get-services">Edit</button>
                               <button class="btn btn-material delete" data-id="{{$tab->id}}" data-page="tabs">Delete</button>
                           </td>
                       </tr>
                   @endforeach
                   </tbody>
               </table>

                @include('admin.tabs.modal.add')
                @include('admin.tabs.modal.edit')

                <h2>Services</h2>
                <button class="btn btn-material add-modal-service">Create</button>
                <table id="dataTableServicies" class="mdl-data-table" style="width:100%">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>Image</th>
                        <th>Title</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($servicies as $service)
                        <tr>
                            <td id="service-id">{{$service->id}}</td>
                            <td id="service-image"><img src="{{$service->image}}" id="img" width="200px"></td>
                            <td id="service-title">{{$service->title}}</td>
                            <td id="service-description">{{$service->description}}</td>
                            <td>
                                <button class="btn btn-material edit-modal-service">Edit</button>
                                <button class="btn btn-material delete" data-id="{{$service->id}}" data-page="servicies">Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @include('admin.servicies.modal.add')
                @include('admin.servicies.modal.edit')

            </div>
        </div>
    </div>
@stop

@section('css')
    <link href="{{asset('vendor/adminlte/css/style.min.css')}}" rel="stylesheet">
@stop

@section('js')
    <script type="text/javascript" src="{{asset('vendor/adminlte/js/dataTables.material.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/adminlte/js/uploadImageAdd.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/adminlte/js/ajaxCrud.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/adminlte/js/dataTable.js')}}"></script>
    <script type="text/javascript" src="{{asset('vendor/adminlte/js/toastr.min.js')}}"></script>--}}
@stop