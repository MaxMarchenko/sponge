<!-- Modal form to add a post -->
<div id="addModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Tab name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" name="name" id="name_add" autofocus>
                            <p class="errorName text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="description" id="description_add" cols="40" rows="5"></textarea>
                            <p class="errorDescription text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Select services:</label>
                        <div class="col-sm-10">
                            {{Form::select('servicies[]',$dataServisies, null, ['class' => 'js-example-basic-multiple', "id" => "servicies_add", 'multiple'=>'multiple','data-placeholder'=>'Select services'])}}
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material add" data-dismiss="modal">
                        <span id="" class='glyphicon glyphicon-check'></span> Add
                    </button>
                    <button type="button" class="btn btn-material" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
