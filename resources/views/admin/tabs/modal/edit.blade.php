<!-- Modal form to edit a form -->
<div id="editModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">×</button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="id">ID:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="id_edit" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="title">Tab name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="name_edit" autofocus>
                            <p class="errorName text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Description:</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="description_edit" cols="40" rows="5"></textarea>
                            <p class="errorDescription text-center alert alert-danger hidden"></p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="content">Select services:</label>
                        <div class="col-sm-10">
                            {{Form::select('servicies[]',$dataServisies, null/*$tabs->service->pluck('id') */,
                                ['class' => 'js-example-basic-multiple',
                                "id" => "servicies_edit",
                                'multiple'=>'multiple',
                                'data-placeholder'=>
                                'Select services',
                                'style' => 'width: 100%']
                            )}}
                        </div>
                    </div>
                </form>
                <div class="modal-footer">
                    <button type="button" class="btn btn-material edit" data-dismiss="modal">
                        <span class='glyphicon glyphicon-check'></span> Edit
                    </button>
                    <button type="button" class="btn btn-material" data-dismiss="modal">
                        <span class='glyphicon glyphicon-remove'></span> Close
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>