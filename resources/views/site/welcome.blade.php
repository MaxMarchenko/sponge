<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Test for Sponge Digital & Design</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{asset('css/jquery.fancybox.css')}}" media="screen" />
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
</head>

<body>
    <div class="navbar navbar-fixed-top" role="navigation" data-0="line-height:100px; height:100px; background-color:rgba(0,0,0,0.3);" data-300="line-height:60px; height:60px; background-color:rgba(0,0,0,1);">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="fa fa-bars color-white"></span>
                </button>
                <h1><a class="navbar-brand" href="/" data-0="line-height:90px;" data-300="line-height:50px;">Test for Sponge Digital & Design
                    </a></h1>
            </div>
            <div class="navbar-collapse collapse">
                <ul class="nav navbar-nav" data-0="margin-top:20px;" data-300="margin-top:5px;">
                    <li class="active"><a href="/">Home</a></li>

                    @foreach($tabs as $tab)
                        <li><a href="#{{$tab->name}}">{{$tab->name}}</a></li>
                    @endforeach
                    @if (Route::has('login'))
                            @auth
                                @if(Auth::user()->canDo(App\Core\Models\Permission::VIEW_ADMIN))
                                    <li><a href="{{ url('/admin') }}">Admin</a></li>
                                @else
                                    <li><a href="{{ url('/') }}">Home</a></li>
                                @endif
                            @else
                                <li><a href="{{ route('login') }}">Login</a></li>
                            @endauth
                    @endif
                </ul>
            </div>
        </div>
    </div>

    @foreach($tabs as $tab)
        <section class="featured" id="{{$tab->name}}">
            <div class="container">
                <div class="row mar-bot40">
                    <div class="col-md-6 col-md-offset-3">

                        <div class="align-center">
                            <h2 class="slogan">{{$tab->name}}</h2>
                            <p>
                                {{$tab->description}}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section id="section-services" class="section pad-bot30 bg-white">
            <div class="container">

                <div class="row mar-bot40">
                    @foreach($tab->service as $service)
                        <div class="col-lg-4">
                            <div class="align-center">
                                <img src="{{$service->image}}" alt="" width="200px"><br><br>
                                <h4 class="text-bold">{{$service->title}}</h4>
                                <p>{{$service->description}}</p>
                            </div>
                        </div>
                    @endforeach

                </div>

            </div>
        </section>
    @endforeach

    <script src="{{asset('js/modernizr-2.6.2-respond-1.1.0.min.js')}}"></script>
    <script src="{{asset('js/jquery.js')}}"></script>
    <script src="{{asset('js/jquery.easing.1.3.js')}}"></script>
    <script src="{{asset('js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/fancybox/jquery.fancybox.pack.js')}}"></script>
    <script src="{{asset('js/skrollr.min.js')}}"></script>
    <script src="{{asset('js/jquery.scrollTo.js')}}"></script>
    <script src="{{asset('js/jquery.localScroll.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
</body>
</html>
