<?php

namespace App\Http\Controllers\Admin;

use App\Core\Repositories\ServiceRepository;
use App\Core\Services\ImageService;
use App\Core\Services\Service;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use Illuminate\Http\Request;
use App\Core\Models\Service as ModelService;

class ServiceController extends Controller
{

    protected $serviceRep;
    protected $service;
    protected $imageService;

    public function __construct(ServiceRepository $serviceRep, Service $service, ImageService $imageService)
    {
        $this->serviceRep = $serviceRep;
        $this->service = $service;
        $this->imageService = $imageService;
    }//__construct

    /**
     * Store a newly created resource in storage.
     *
     * @param ServiceRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ServiceRequest $request)
    {
        $service = $this->service->createService($request->all());
        $this->service->saveImage($request->image, $service);
        return response()->json($request);
    }//store


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = ModelService::find($id);
        $service->update($request->all());
        $this->service->saveImage($request->image, $service);

        return response()->json($request);
    }//update

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $img = $request->img;
        if ($this->service->checkDeletedService($img, $id))
        {
            return response()->json(['status' => 'success']);
        }

    }//destroy

    public function uploadImage(Request $request)
    {
        $image = $request->file('image');
        return $this->imageService->uploadImage($image);
    }//uploadImage
}
