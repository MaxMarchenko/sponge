<?php

namespace App\Http\Controllers\Admin;

use App\Core\Models\Tab;
use App\Core\Repositories\ServiceRepository;
use App\Core\Repositories\TabRepository;
use App\Core\Services\Service;
use App\Core\Services\TabService;
use App\Http\Controllers\Controller;
use App\Http\Requests\TabRequest;
use Illuminate\Http\Request;

class TabController extends Controller
{

    protected $tabRep;
    protected $tabService;
    protected $service;
    protected $serviceRep;

    public function __construct(TabRepository $tabRep, TabService $tabService, ServiceRepository $serviceRep, Service $service)
    {
        $this->tabRep = $tabRep;
        $this->tabService = $tabService;
        $this->serviceRep = $serviceRep;
        $this->service = $service;
    }//__construct

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $tabs = $this->tabService->getAllTabs();
        $servicies = $this->service->getAllServicies();
        $dataServisies = $this->serviceRep->generateDataArray('title', 'id');

        return view('admin.tabs.index', ['tabs' => $tabs, 'servicies' => $servicies, 'dataServisies' => $dataServisies]);
    }//index

    /**
     * Store a newly created resource in storage.
     *
     * @param TabRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(TabRequest $request)
    {
        $tab = $this->tabService->createTab($request->all());
        $tab->setServicies($request->get('servicies'));

        return response()->json($request);
    }//store

    /**
     * Update the specified resource in storage.
     *
     * @param TabRequest $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(TabRequest $request, $id)
    {
        $tab = $this->tabRep->getOne($id);
        $tab->update($request->all());
        $tab->setServicies($request->get('servicies'));

        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @param  int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        if ($this->tabService->deleteTab($id)) {
            return response()->json(['status' => 'success']);
        }

        return response()->json(['status' => 'failed']);
    } // destroy
}
