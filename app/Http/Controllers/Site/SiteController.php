<?php

namespace App\Http\Controllers\Site;

use App\Core\Repositories\ServiceRepository;
use App\Core\Repositories\TabRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SiteController extends Controller
{

    protected $tabRep;

    public function __construct(TabRepository $tabRep)
    {
        $this->tabRep = $tabRep;
    }//__construct

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tabs = $this->tabRep->getAll();
        return view('site.welcome', ['tabs' => $tabs]);
    }
}
