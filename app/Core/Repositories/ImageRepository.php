<?php

namespace App\Core\Repositories;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ImageRepository extends ADefaultRepository
{

    /**
     * TabController constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }//__construct

    public static function getInstance(Model $model) {
        return new self($model);
    }//getInstance

    /**
     * @param $image
     * @param $path
     */
    public function saveImage($image)
    {
        if($image == null) { return; }

        $this->removeImage();

        $this->model->image = $image;
        $this->model->save();
    }//saveImage

    /**
     * @param $path
     * @return bool|null
     */
    private function removeImage()
    {
        return ($this->model->image !== null) ? Storage::delete($this->model->image): null;
    }//removeImage
}