<?php

namespace App\Core\Repositories;

use App\Core\Models\Service;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class ServiceRepository extends ADefaultRepository
{

    protected $serviceRep;

    /**
     * TabController constructor.
     * @param Service $model
     */
    public function __construct(Service $model)
    {
        parent::__construct($model);
    }//__construct

    public function add($fields)
    {
        $service = new Service();
        $service->fill($fields);
        $service->save();

        return $service;
    }//add

    public function delete($id)
    {
        $this->model->find($id)->tabs()->detach();
        return $this->model->destroy($id);
    }//delete
}