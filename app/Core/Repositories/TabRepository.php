<?php

namespace App\Core\Repositories;

use App\Core\Models\Tab;

class TabRepository extends ADefaultRepository {

    protected $tabRep;
    /**
     * TabController constructor.
     * @param Tab $model
     */
    public function __construct(Tab $model)
    {
        parent::__construct($model);
    }//__construct

    public function add($fields)
    {
        $post = new Tab();
        $post->fill($fields);
        $post->save();

        return $post;
    }//add

    public function delete($id)
    {
        $this->model->find($id)->service()->detach();
        return $this->model->destroy($id);
    }//delete
}