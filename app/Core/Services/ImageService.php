<?php

namespace App\Core\Services;


use App\Core\Repositories\ImageRepository;

class ImageService
{
//    protected $imageRep;
//    protected $permRep;
//
//    public function __construct(ImageRepository $imageRep)
//    {
//        $this->imageRep = $imageRep;
//    }//__construct

    public function uploadImage($image) {
        if (isset($image))
        {
            $filename = str_random(10) . '.' . $image->extension();
            $image->storeAs('images', $filename);

            return ['file' => $filename];
        }
    }//uploadImage
}