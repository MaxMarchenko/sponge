<?php

namespace App\Core\Services;

use App\Core\Models\Tab;
use App\Core\Repositories\TabRepository;

class TabService
{
    protected $tabRep;

    public function __construct(TabRepository $tabRep)
    {
        $this->tabRep = $tabRep;
    }//__construct

    public function getAllTabs()
    {
        return $this->tabRep->getAll();
    }//getAllTabs

    public function createTab($fields)
    {
        return $this->tabRep->add($fields);
    }//createTab

    public function deleteTab($id) {
        return $this->tabRep->delete($id);
    }//deleteTab

}//PermissionService