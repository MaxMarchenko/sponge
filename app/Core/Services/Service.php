<?php

namespace App\Core\Services;


use App\Core\Repositories\ImageRepository;
use App\Core\Repositories\ServiceRepository;
use Illuminate\Support\Facades\Storage;

class Service
{
    protected $serviceRep;

    public function __construct(ServiceRepository $serviceRep)
    {
        $this->serviceRep = $serviceRep;
    }//__construct

    public function getAllServicies()
    {
        return $this->serviceRep->getAll();
    }//getAllServicies

    public function createService($fields)
    {
        return $this->serviceRep->add($fields);
    }//createService

    public function saveImage($image, $object) {
        return ImageRepository::getInstance($object)->saveImage($image);
    }//uploadImage

    public function deleteService($id) {
        return $this->serviceRep->delete($id);
    }//deleteTab

    public function removeImage($img)
    {
        return ($img != null) ? Storage::delete($img): true;
    }//removeImage

    public function checkDeletedService($img, $id)
    {
        return $this->deleteService($id) && $this->removeImage($img);
    }//checkDeletedService
}