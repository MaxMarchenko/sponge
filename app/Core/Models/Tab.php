<?php

namespace App\Core\Models;
use Illuminate\Database\Eloquent\Model;


class Tab extends Model
{

    protected $fillable = [
        'name', 'description'
    ];

    public function service() {
        return $this->belongsToMany(
            Service::class,
            'service_tab',
            'tab_id',
            'service_id'
        );
    }//product

    /**
     * @param $ids
     * @return array|null
     */
    public function setServicies($ids) {
        if ($ids == null) {return null;}
        return $this->service()->sync($ids);
    }//setServicies

    public function getServices()
    {
        return implode(', ', $this->service()->pluck('title')->all());
    }//getServices
}
