<?php

namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{

    protected $fillable = [
        'description', 'title',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tabs() {
        return $this->belongsToMany(
            Tab::class,
            'service_tab',
            'service_id',
            'tab_id'
        );
    }
}
