<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServiceTabTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('service_tab', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tab_id')->unsigned()->default(1);
            $table->foreign('tab_id')->references('id')->on('tabs');
            $table->integer('service_id')->unsigned()->default(1);
            $table->foreign('service_id')->references('id')->on('services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('service_tab');
    }
}
