<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdminUserToUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('users')->insert([
                [
                    'id'         => 1,
                    'name'       => 'admin',
                    'email'      => 'admin@gmail.com',
                    'password'   => '$2a$04$KGZN//VHWmMTfh3ib5.bHO0VYWJIWp0bgR3RnQ5BqM21gwC1EeMKS',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('users')->delete(1);
    }
}
