<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Core\Models\Permission;

Route::get('/', 'Site\SiteController@index')->name('site');
Auth::routes();

Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth', 'admin:'. Permission::VIEW_ADMIN]], function() {
	// /admin
	Route::get('/', 'IndexController@index')->name('adminIndex');
    Route::get('/home', 'HomeController@index')->name('home');

	Route::get('/permissions', 'PermissionsController@index')->name('perms');
	Route::post('/permissions', 'PermissionsController@edit')->name('perms-edit');
    Route::resource('/tabs', 'TabController');
    Route::resource('/servicies', 'ServiceController');
    Route::post('servicies/upload_image', 'ServiceController@uploadImage');
});



Route::group(['prefix' => 'errors', 'namespace' => 'Errors'], function() {
    Route::get('/permission_denied', 'PermissionController@permissionDenied')->name('permission_denied');
});